/* eslint no-console: 0 */
/* global Promise */
class User {

  constructor(name){
    this.name = name;
  }

  async save() {
    return new Promise((resolve/*, reject*/) => {
      console.log(`start saving User = ${this.name}`);
      setTimeout(() => {
        console.log(`end saving User = ${this.name}`);
        resolve();
      }, 500);
    });
  }
}

module.exports = User;