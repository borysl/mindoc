const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const { log, ExpressAPILogMiddleware } = require('@rama41222/node-logger');
const { name } = require('./package.json');

var port = process.env.PORT || 3000;

process.title = name;

const config = {
  name: name,
  port: port,
  host: '0.0.0.0',
};

const app = express();
const logger = log({ console: true, file: false, label: config.name });

app.use(bodyParser.json());
app.use(cors());
app.use(ExpressAPILogMiddleware(logger, { request: true }));

app.get('/', (req, res) => {
  logger.info(`${config.name} running on ${config.host}:${config.port}`);
  res.status(200).send('hello world');
});

const checkAndHandleError = (err) => {
  if (!err) return;
  logger.error(`Error: ${err.message}`);
  throw new Error('Internal Server Error');
};

app.get('/code', (req, res) => {
  fs.readFile('server.js', (err, data) => {
    checkAndHandleError(err);
    res.status(200).type('text').send(data);
  });
});

app.listen(config.port, config.host, (err) => {
  checkAndHandleError(err);
  logger.info(`${config.name} running on ${config.host}:${config.port}`);
});