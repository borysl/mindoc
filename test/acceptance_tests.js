/* global describe, it, before, after */
/* eslint no-console: 0 */ 

const supertest = require('supertest');
const should = require('should');
const child_process = require('child_process');  
const server = supertest.agent('http://localhost:3000');

let serverProcess;

describe('Acceptance tests', () => {
  before((initialized) => {
    console.log('before script');
    serverProcess = child_process.fork('server.js');
    serverProcess.on('close', function (code) {  
      console.log('child process exited with code ' + code);  
    });
    setTimeout(() => {
      console.log('1s elapsed');
      initialized();
    }, 1000);
  });

  after(function() {
    console.log('after script');
    serverProcess.kill('SIGINT');
  });

  it('should return hello page', (done) => {

    server
      .get('/')
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err,res) => {
        if (err) return done(err);
        res.text.should.equal('hello world');
        done();
      });
  });

  it('should return server.js file content', (done) => {
    server
      .get('/code')
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err,res) => {
        if (err) return done(err);
        let includes = res.text.indexOf('app.use(ExpressAPILogMiddleware(logger, { request: true }));') > -1;
        should(includes).be.true();
        done();
      });
  });
});